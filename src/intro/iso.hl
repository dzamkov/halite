open eq in
record

	-- : An injective function from a value of type [A] to a value of type [B]
	val (A : type) ~> (B : type) = struct
		val f : A -> B
		val inj : {a : A} -> {b : B} -> (f a ~ f b) -> (a ~ b)
	end

	-- | Applies an injective function, [f], to an argument, [a]
	val (f : A ~> B) : A -> B = f.f

	-- : An isomorphism between values of type [A] and values of type [B]
	val (A : type) <~> (B : type) = struct
		val f : A -> B
		val g : B -> A
		val invl : {a : A} -> (g (f a) ~ a)
		val invr : {a : A} -> (f (g a) ~ a)
	end

	-- | Reverses the direction of the isomorphism [iso]
	val rev (iso : A <~> B) : B <~> A = record
		val f = iso.g
		val g = iso.f
		val invl = iso.invr
		val invr = iso.invl
	end

	-- | The injective function which applies the isomorphism [iso] from left to right
	val appl (iso : A <~> B) : A ~> B = record
		val f = iso.f
		val inj = (a : A). (b : B). (u : f a ~ f b).
			trans (symm (iso.invl a)) (trans (cong g u) (iso.invl b))
	end

	-- | The injective function which applies the isomorphism [iso] from right to left
	val appr (iso : A <~> B) : B ~> A = appl (rev iso)
end