record

	-- : A boolean value
	val Bool : type = newtype {A : type} -> A -> A -> A

	-- | The false boolean
	val false : Bool = (A : type). (t : A). (f : A). f

	-- | The true boolean
	val true : bool = (A : type). (t : A). (f : A). t

	-- | The negation of [x]
	val not (x : Bool) : Bool = x Bool false true

	-- | The logical conjunction of [x] and [y]
	val (x : Bool) & (y : Bool) : Bool = x Bool y false

	-- | The logical disjunction of [x] and [y]
	val (x : Bool) | (y : Bool) : Bool = x Bool true y

	-- | Decides whether the two booleans, [x] and [y], are equal
	val (x : Bool) == (y : Bool) : Bool = x Bool y (not y)
end