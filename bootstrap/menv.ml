module UniqueSet = Set.Make (Unique)
module UniqueMap = Map.Make (Unique)

(* A set of values of type 'a paired with conditions where they are applicable. This type
is monadic. *)
type 'a t =
	UniqueSet.t -> Expr.sub_map ->
	(UniqueSet.t -> Expr.sub_map -> 'a -> unit) ->
	unit

(* The empty environment *)
let none : 'a t = fun _ _ _ -> ()

(* An environment where the given value is always applicable, and no other value is ever
applicable *)
let pure (value : 'a) : 'a t = fun vars map post -> post vars map value

(* Takes the union of the values and conditions between two environments *)
let union (a : 'a t) (b : 'a t) : 'a t =
	fun vars map post ->
		a vars map post;
		b vars map post

(* The environment of all expressions, of any type *)
let all : Expr.t t =
	fun vars map post ->
		let u = Unique.create () in
		let v = Expr.var_ext u in
		post (UniqueSet.add u vars) map v

(* The environment of all expressions of the given type *)
let all_of_type (typ : Expr.t) : Expr.t t =
	fun vars map post ->
		let u = Unique.create () in
		let v = Expr.var_ext u in
		post (UniqueSet.add u vars) (UniqueMap.add u (Expr.HasType typ) map) v

exception UniqueFail

(* An interface for treating a value as if it were an expression *)
type 'a expr_like = {
	shift_subs_map : int -> Expr.sub_map -> 'a -> 'a;
	occurs_ext_set : UniqueSet.t -> 'a -> bool;
	eq : 'a -> 'a -> bool } 

(* An 'expr_like' interface for an expression *)
let expr_expr_like = {
	shift_subs_map = Expr.shift_subs_map;
	occurs_ext_set = Expr.occurs_ext_set;
	eq = Expr.eq }

(* An 'expr_like' interface for a tuple of two 'expr_like' values. *)
let tuple_expr_like a_expr_like b_expr_like = {
	shift_subs_map = (fun amount sub_map (a, b) ->
		(a_expr_like.shift_subs_map amount sub_map a,
		b_expr_like.shift_subs_map amount sub_map b));
	occurs_ext_set = (fun set (a, b) ->
		a_expr_like.occurs_ext_set set a ||
		b_expr_like.occurs_ext_set set b);
	eq = (fun (l_a, l_b) (r_a, r_b) ->
		a_expr_like.eq l_a r_a &&
		b_expr_like.eq l_b r_b) }

(* If the given inner environment can be shown to contain exactly one
expression-like value, this will return that expression. Otherwise, this will
return 'None'. *)
let collapse (expr_like : 'a expr_like) (inner : 'a t) : 'a option t =
	fun vars map post ->
		let res = ref None in
		let inner_post i_vars i_map i_expr =
			let d_vars = UniqueSet.diff i_vars vars in
			let n_i_vars = i_vars in (* TODO *)
			let n_i_map = UniqueMap.filter (fun u _ -> not (UniqueSet.mem u d_vars)) i_map in
			let n_i_expr = expr_like.shift_subs_map 0 i_map i_expr in
			if expr_like.occurs_ext_set d_vars n_i_expr
			then raise UniqueFail
			else match !res with
				| Some (c_vars, c_map, c_expr) when not (expr_like.eq n_i_expr c_expr) ->
					raise UniqueFail
				| None ->
					res := Some (n_i_vars, n_i_map, n_i_expr)
				| _ -> () in
		try inner vars map inner_post;
			match !res with
				| Some (c_vars, c_map, c_expr) -> post c_vars c_map (Some c_expr)
				| None -> ()
		with UniqueFail -> post vars map None

(* If the given inner environment can be shown to contain exactly one expression, this
will return that expression. Otherwise, this will return 'None'. *)
let collapse_expr = collapse expr_expr_like

(* Takes the cartesian product of the values in two environements where the conditions
are compatible, using the value of the second as the final value. *)
let (>>) (a : 'a t) (b : 'b t) : 'b t =
	fun vars map post ->
		a vars map (fun a_vars a_map _ ->
			b a_vars a_map post)

(* Takes the cartesian product of the values in two environements where the conditions
are compatible, applying the second value to the first to get the final value. *)
let (>>=) (a : 'a t) (f : 'a -> 'b t) : 'b t =
	fun vars map post ->
		a vars map (fun a_vars a_map a_val ->
			f a_val a_vars a_map post)

(* An environment which contains a value only under the condition that the two given
expression can be positively determined to be equal. *)
let unify (a : Expr.t) (b : Expr.t) : unit t =
	let sub_valid vars i x =
		UniqueSet.mem i vars &&
		not (Expr.occurs_ext_set (UniqueSet.singleton i) x) 
	in let rec unify_sub a b vars map post =
		(match (Expr.reduced_head a, Expr.reduced_head b) with
			| (Expr.Var (Expr.Int i), Expr.Var (Expr.Int j)) ->
				if i = j then post vars map () else ()
			| (Expr.Var (Expr.Ext u), Expr.Var (Expr.Ext v)) when u = v ->
				post vars map ()
			| (Expr.Var (Expr.Ext u), _) when sub_valid vars u b ->
				post vars (Expr.sub_map_add u (Expr.HasValue b) map) ()
			| (_, Expr.Var (Expr.Ext u)) when sub_valid vars u a ->
				post vars (Expr.sub_map_add u (Expr.HasValue a) map) ()
			| (Expr.TypeOf a, b_head) -> (match Expr.reduced_head a with
				| Expr.Var (Expr.Ext u) when sub_valid vars u b -> 
					post vars (Expr.sub_map_add u (Expr.HasType b) map) ()
				| _ -> (match b_head with
					| Expr.TypeOf b -> unify_sub a b vars map post (* Approx *)
					| _ -> ()))
			| (_, Expr.TypeOf b) -> (match Expr.reduced_head b with
				| Expr.Var (Expr.Ext u) when sub_valid vars u a -> 
					post vars (Expr.sub_map_add u (Expr.HasType a) map) ()
				| _ -> ())
			| (Expr.Axiom a, Expr.Axiom b) -> if a = b then post vars map () else ()
			| (Expr.App (a_f, a_a), Expr.App (b_f, b_a)) ->
				(unify_sub a_f b_f >> unify_c_sub map a_a b_a) vars map post (* Approx *)
			| (Expr.Abs (a_a, a_b), Expr.Abs (b_a, b_b)) ->
				(unify_sub a_a b_a >> unify_c_sub map a_b b_b) vars map post (* Approx *)
			| (Expr.Prod (a_a, a_r), Expr.Prod (b_a, b_r)) ->
				(unify_sub a_a b_a >> unify_c_sub map a_r b_r) vars map post
			| (Expr.Cast (a_v, a_t), Expr.Cast (b_v, b_t)) ->
				(unify_sub a_t b_t >> unify_c_sub map a_v b_v) vars map post
			| _ -> ())
	and unify_c_sub c_sub a b vars map post =
		if c_sub == map then unify_sub a b vars map post
		else unify_f_sub a b vars map post
	and unify_f_sub a b vars map =
		unify_sub
		(Expr.subs_map map a)
		(Expr.subs_map map b)
		vars map
	in unify_f_sub a b

(* An environment which contains a value only under the condition that the external
variable of the given index has the given type *)
let var_ext_has_type (var_u : Unique.t) (typ : Expr.t) : unit t =
	(* unify (Expr.typeof (Expr.var_ext var_u)) typ *)
	fun vars map post ->
		try match UniqueMap.find var_u map with
			| Expr.HasValue v -> unify (Expr.typeof v) typ vars map post
			| Expr.HasType t -> unify t typ vars map post
		with Not_found -> post vars (UniqueMap.add var_u (Expr.HasType typ) map) ()

(* Helper for creating an abstraction expression within an environment *)
let abs arg_typ func : Expr.t t =
	let var_u = Unique.create () in
	let var = Expr.var_ext var_u in
	var_ext_has_type var_u arg_typ >>
	func var >>= fun body ->
	(fun vars map post ->
		let i_map = Expr.sub_map_add var_u (Expr.HasValue (Expr.var (Expr.Int 0))) map in
		let n_body = Expr.subs_map i_map body in
		post vars map (Expr._abs arg_typ n_body))

let run_varless (inner : 'a t) : 'a option =
	let res = ref None in
	let inner_post vars map value =
		if UniqueSet.is_empty vars
		then match !res with
			| Some _ -> raise UniqueFail
			| None -> res := Some value
		else raise UniqueFail in
	try inner UniqueSet.empty UniqueMap.empty inner_post; !res
	with UniqueFail -> None