open Expr
open Menv
module StringMap = Map.Make (String)

type conv = Expr.t -> Expr.t -> Expr.t Menv.t

type func = {
	arg_typ : Expr.t;
	app : Expr.t -> Expr.t }

type sym =
	| Auto
	| Ident of Grammar.ident
	| Prefix of Grammar.monop * Expr.t
	| Postfix of Expr.t * Grammar.monop
	| Infix of Expr.t * Grammar.binop * Expr.t

let sym_expr_like = {
	subs_map = (fun sub_map sym -> match sym with
		| Auto -> sym
		| Ident _ -> sym
		| Prefix (op, arg) -> Prefix (op, subs_map sub_map arg)
		| Postfix (arg, op) -> Postfix (subs_map sub_map arg, op)
		| Infix (l_arg, op, r_arg) -> Infix (
				subs_map sub_map l_arg, op,
				subs_map sub_map r_arg));
	occurs_ext_set = (fun set sym -> match sym with
		| Auto -> false
		| Ident _ -> false
		| Prefix (_, arg) -> occurs_ext_set set arg
		| Postfix (arg, _) -> occurs_ext_set set arg
		| Infix (l_arg, _, r_arg) -> occurs_ext_set set l_arg || occurs_ext_set set r_arg);
	eq = (fun l_sym r_sym -> match (l_sym, r_sym) with
		| (Auto, Auto) -> true
		| (Ident l_id, Ident r_id) -> l_id = r_id
		| (Prefix (l_op, l_arg), Prefix (r_op, r_arg)) -> l_op = r_op && eq l_arg r_arg
		| (Postfix (l_arg, l_op), Postfix (r_arg, r_op)) -> l_op = r_op && eq l_arg r_arg
		| (Infix (l_l_arg, l_op, l_r_arg), Infix (r_l_arg, r_op, r_r_arg)) ->
			l_op = r_op && eq l_l_arg r_l_arg && eq l_r_arg r_r_arg
		| _ -> false) }
	
type field = {
	bkg_var_typs : Expr.t list;
	sym : sym }

module Scope = struct
	type t = {
		auto : Expr.t Menv.t;
		idents : Expr.t Menv.t StringMap.t;
		prefixes : (conv -> Expr.t -> Expr.t Menv.t) StringMap.t;
		postfixes : (conv -> Expr.t -> Expr.t Menv.t) StringMap.t;
		infixes : (conv -> Expr.t -> Expr.t -> Expr.t Menv.t) StringMap.t; }

	let empty = {
		auto = none;
		idents = StringMap.empty;
		prefixes = StringMap.empty;
		postfixes = StringMap.empty;
		infixes = StringMap.empty }

	let union a b =
		if a == empty then b
		else if b == empty then a
		else
			let combine f _ x y = (match (x, y) with
				| (Some x, Some y) -> Some (f x y)
				| (Some x, None) -> Some x
				| (None, Some y) -> Some y
				| (None, None) -> None) in
			let f_monop a b conv op =
				Menv.union (a conv op) (b conv op) in
			let f_binop a b conv l_arg r_arg =
				Menv.union (a conv l_arg r_arg) (b conv l_arg r_arg) in
			{ auto = Menv.union a.auto b.auto;
			idents = StringMap.merge (combine Menv.union) a.idents b.idents;
			prefixes = StringMap.merge (combine f_monop) a.prefixes b.prefixes;
			postfixes = StringMap.merge (combine f_monop) a.postfixes b.postfixes;
			infixes = StringMap.merge (combine f_binop) a.infixes b.infixes }

	let ident name expr = { empty with
		idents = StringMap.singleton name expr; }

	let infix op func = { empty with
		infixes = StringMap.singleton op func; }

	let union_ident name expr scope = union (ident name expr) scope

	let from_field field value =
		let inst_bkg f = match field.bkg_var_typs with
			| [] -> pure (f Expr.empty_context)
			| _ ->
				inst_bkg_vars field.bkg_var_typs >>= fun int_subs ->
				Menv.rebuild int_subs f in
		match field.sym with
		| Auto ->
			let inst = inst_bkg (fun context -> Expr.rebuild context value) in
			{ empty with auto = inst }
		| Ident name ->
			let inst = inst_bkg (fun context -> Expr.rebuild context value) in
			{ empty with idents = StringMap.singleton name inst }
		| Prefix (op, arg_typ) ->
			let inst conv arg =
				inst_bkg (fun context ->
					(Expr.rebuild context value,
					Expr.rebuild context arg_typ)) >>= fun (value, arg_typ) ->
				conv arg_typ arg >>= fun arg ->
				pure (Expr.app value arg) in
			{ empty with prefixes = StringMap.singleton op inst }
		| Postfix (arg_typ, op) ->
			let inst conv arg =
				inst_bkg (fun context ->
					(Expr.rebuild context value,
					Expr.rebuild context arg_typ)) >>= fun (value, arg_typ) ->
				conv arg_typ arg >>= fun arg ->
				pure (Expr.app value arg) in
			{ empty with postfixes = StringMap.singleton op inst }
		| Infix (l_arg_typ, op, r_arg_typ) ->
			let inst conv l_arg r_arg =
				inst_bkg (fun context ->
					(Expr.rebuild context value,
					Expr.rebuild context l_arg_typ,
					Expr.rebuild context r_arg_typ)) >>= fun (value, l_arg_typ, r_arg_typ) ->
				conv l_arg_typ l_arg >>= fun l_arg ->
				conv r_arg_typ r_arg >>= fun r_arg ->
				pure (Expr.app (Expr.app value l_arg) r_arg) in
			{ empty with infixes = StringMap.singleton op inst }

	let default = List.fold_left union empty [
		ident "type" (pure (Expr.ax Expr.Type));
		infix "->" (fun conv l r -> pure (Expr.prod_const l r))]

	let conv (scope : t) : conv = fun typ value ->
		unify typ (Expr.typeof value) >> pure value (* TODO: Conversions *)

	let conv_func (scope : t) (expr : Expr.t) : func Menv.t =
		all >>= fun arg_typ -> (* TODO: Conversions *)
		all >>= fun ret_typ ->
		unify (Expr.typeof expr) (Expr._prod arg_typ ret_typ) >>
		pure { arg_typ = arg_typ; app = fun arg -> Expr.app expr arg }

	let get_auto scope = scope.auto

	let try_get_ident scope name =
		try Some (StringMap.find name scope.idents)
		with Not_found -> None

	let get_ident scope name = match try_get_ident scope name with
		| Some ident -> ident
		| None -> none

	let get_prefix scope op arg =
		try StringMap.find op scope.prefixes (conv scope) arg
		with Not_found -> none

	let get_postfix scope arg op =
		try StringMap.find op scope.postfixes (conv scope) arg
		with Not_found -> none

	let get_infix scope l_arg op r_arg =
		try StringMap.find op scope.infixes (conv scope) l_arg r_arg
		with Not_found -> none
end

module Bkg = struct
	type t = { bkg_vars : (string, Unique.t) Hashtbl.t }

	let get_ident bkg name : Expr.t Menv.t =
		let u =
			try Hashtbl.find bkg.bkg_vars name
			with Not_found ->
				let u = Unique.create () in
				Hashtbl.add bkg.bkg_vars name u; u in
		fun vars map post ->
			try (match UniqueMap.find u map with
				| HasType _ -> post vars map (Expr.var_ext u)
				| HasValue _ -> failwith "Unexpected")
			with Not_found ->
				let v = Unique.create () in
				post (UniqueSet.add v vars)
					(UniqueMap.add u (HasType (Expr.var_ext v)) map)
					(Expr.var_ext u)
	
	let use (expr_like : 'a expr_like) (inner : t -> 'a Menv.t) : (Expr.t list * 'a) Menv.t =
		let bkg = {	bkg_vars = Hashtbl.create 7 } in
		fun vars map post ->
			let inner_post i_vars i_map i_expr =
				match
					Hashtbl.fold (fun _ u accum -> match accum with
						| Some accum ->
							(try match UniqueMap.find u i_map with
								| HasValue _ -> None
								| HasType typ -> Some (UniqueMap.add u typ accum)
							with Not_found -> None)
						| None -> None) bkg.bkg_vars (Some UniqueMap.empty)
						(* TODO: fail if vars sub to a bkg var *)
				with
					| Some bkgs ->
						let rec order i accum_subs accum_typs bkgs =
							let res = ref None in
							if UniqueMap.is_empty bkgs
							then Some (i, accum_subs, accum_typs)
							else if (UniqueMap.exists (fun k typ ->
								if Expr.occurs_ext_map_key bkgs typ
								then false
								else (res := Some (k, typ); true)) bkgs);
							then
								(match !res with
									| Some (k, typ) ->
										order
											(i + 1)
											(UniqueMap.add k (HasValue (var (Int i))) accum_subs)
											((Expr.shift_subs_map i accum_subs typ) :: accum_typs)
											(UniqueMap.remove k bkgs)
									| None -> None)
							else None
						in (match order 0 UniqueMap.empty [] bkgs with
							| Some (i, subs, typs) ->
								let n_i_map =	UniqueMap.filter
									(fun u _ -> not (UniqueMap.mem u subs)) i_map in
								post i_vars n_i_map (List.rev typs, expr_like.shift_subs_map i subs i_expr)
							| None -> ())
					| None -> ()
			in inner bkg vars map inner_post


		(* sub_map (fun map ->
			match
				Hashtbl.fold (fun _ (u, u_typ) accum -> match accum with
					| Some accum ->
						(try match UniqueMap.find u_typ map with
							| HasValue _ -> None
							| HasType typ -> Some (UniqueMap.add u typ accum)
						with Not_found -> None)
					| None -> None) bkg_tbl (Some UniqueMap.empty)
			with
				| Some p_vars ->
					let rec order accum vars =
						let res = ref None in
						if (UniqueMap.exists (fun k typ ->
							if Expr.occurs (fun var -> match var with
								| Ext e -> UniqueMap.mem e map
								| Int _ -> false) typ
							then false
							else (res := Some (k, typ); true)) vars);
						then
							(match !res with
								| Some (k, typ) -> order ((k, typ) :: accum) (UniqueMap.remove k vars)
								| None -> None)
						else None in 
					(match order [] p_vars with
						| Some o_vars -> ()
						| None -> None)
				| None -> None) >>= fun res ->
			match res with
				| Some res -> pure res
				| None -> impossible *)
end

let get_inner_scope scope container_expr : Scope.t Menv.t = failwith "TODO"

let rec interpret bkg scope (ast : Grammar.expr) : Expr.t Menv.t =
	match ast with
		| Grammar.Ident name -> (match Scope.try_get_ident scope name with
			| Some ident -> ident
			| None -> (match bkg with
				| Some bkg -> Bkg.get_ident bkg name
				| None -> none))
		| Grammar.Sub (container, explicit, sub) ->
			interpret bkg scope container >>= fun container_expr ->
			get_inner_scope scope container_expr >>= fun inner_scope ->
			let n_inner_scope = if explicit then inner_scope else Scope.union scope inner_scope
			in let n_bkg = if explicit then None else bkg
			in interpret n_bkg n_inner_scope sub
		| Grammar.App (f, arg) ->
			interpret bkg scope f >>= fun f_expr ->
			interpret bkg scope arg >>= fun arg_expr ->
			Scope.conv_func scope f_expr >>= fun f_func ->
			Scope.conv scope f_func.arg_typ arg_expr >>= fun arg_val ->
			pure (f_func.app arg_val)
		| Grammar.Abs (var, body) ->
			interpret_var bkg scope var >>= fun (var_name, var_typ_expr) ->
			abs var_typ_expr (fun var_expr -> 
				let inner_scope = Scope.union (Scope.ident var_name (pure var_expr)) scope in
				interpret bkg inner_scope body)
		| Grammar.Typed (v, typ) ->
			interpret bkg scope v >>= fun v_expr ->
			interpret bkg scope typ >>= fun typ_expr ->
			Scope.conv scope typ_expr v_expr
		| Grammar.Prefix (op, arg) ->
			interpret bkg scope arg >>= fun arg_expr ->
			Scope.get_prefix scope op arg_expr
		| Grammar.Postfix (arg, op) ->
			interpret bkg scope arg >>= fun arg_expr ->
			Scope.get_postfix scope arg_expr op
		| Grammar.Infix (l_arg, [(op, r_arg)]) ->
			interpret bkg scope l_arg >>= fun l_arg_expr ->
			interpret bkg scope r_arg >>= fun r_arg_expr ->
			Scope.get_infix scope l_arg_expr op r_arg_expr
		| Grammar.In (scope_dir, body) ->
			interpret_scope_dir scope scope_dir >>= fun inner_scope ->
			interpret bkg inner_scope body
		| _ -> failwith "TODO"

and interpret_var bkg scope (ast : Grammar.expr) : (string * Expr.t) Menv.t =
	match ast with
		| Grammar.Ident name ->
			all >>= fun typ ->
			pure (name, typ)
		| Grammar.Typed (Grammar.Ident name, typ) ->
			interpret bkg scope typ >>= fun typ_expr ->
			pure (name, typ_expr)
		| _ -> failwith "Invalid variable"

and interpret_scope_dir scope (ast : Grammar.scope_directive) : Scope.t Menv.t =
	match ast with
		| Grammar.Open container ->
			interpret None scope container >>= fun container_expr ->
			get_inner_scope scope container_expr >>= fun inner_scope ->
			pure (Scope.union scope inner_scope)
		| Grammar.Let (pattern, typ, value) ->
			interpret_field scope pattern typ value >>= fun (field, value) ->
			let field_scope = Scope.from_field field value in
			pure (Scope.union scope field_scope)
		| _ -> failwith "Invalid scope directive"

and interpret_field scope pattern typ value =
	let sym_expr = Bkg.use (tuple_expr_like sym_expr_like expr_expr_like) (fun bkg ->
		let rec with_subpattern pattern cont = match pattern with
			| Grammar.Ident name ->
				cont scope >>= fun value_expr ->
				pure (Ident name, value_expr)
			| Grammar.App (f, arg) ->
				with_subpattern f (fun n_scope ->
					interpret_var (Some bkg) n_scope arg >>= fun (arg_name, arg_typ_expr) ->
					abs arg_typ_expr (fun arg_expr ->
						let n_n_scope = Scope.union_ident arg_name (pure arg_expr) n_scope in
						cont n_n_scope))
			| Grammar.Prefix (op, arg) ->
				interpret_var (Some bkg) scope arg >>= fun (arg_name, arg_typ_expr) ->
				abs arg_typ_expr (fun arg_expr ->
					let n_scope = Scope.union_ident arg_name (pure arg_expr) scope in
					cont n_scope) >>= fun value_expr ->
				pure (Prefix (op, arg_typ_expr), value_expr)
			| Grammar.Postfix (arg, op) ->
					interpret_var (Some bkg) scope arg >>= fun (arg_name, arg_typ_expr) ->
					abs arg_typ_expr (fun arg_expr ->
						let n_scope = Scope.union_ident arg_name (pure arg_expr) scope in
						cont n_scope) >>= fun value_expr ->
					pure (Postfix (arg_typ_expr, op), value_expr)
			| Grammar.Infix (l_arg, [(op, r_arg)]) ->
				interpret_var (Some bkg) scope l_arg >>= fun (l_arg_name, l_arg_typ_expr) ->
				interpret_var (Some bkg) scope r_arg >>= fun (r_arg_name, r_arg_typ_expr) ->
				abs l_arg_typ_expr (fun l_arg_expr ->
					abs r_arg_typ_expr (fun r_arg_expr ->
						let n_scope = Scope.union_ident l_arg_name (pure l_arg_expr) scope in
						let n_scope = Scope.union_ident r_arg_name (pure r_arg_expr) n_scope in
						cont n_scope)) >>= fun value_expr ->
				pure (Infix (l_arg_typ_expr, op, r_arg_typ_expr), value_expr)
			| _ -> failwith "Invalid pattern"
		in collapse (tuple_expr_like sym_expr_like expr_expr_like)
			(with_subpattern pattern (fun n_scope -> interpret (Some bkg) n_scope value))
			(* TODO: type conversion *)) in
	sym_expr >>= fun (bkg_var_typs, (sym, value_expr)) ->
	pure ({
		bkg_var_typs = bkg_var_typs;
		sym = sym },
		value_expr)