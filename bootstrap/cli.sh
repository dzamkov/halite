#!/bin/sh
ocamlbuild -use-ocamlfind -use-menhir -pkg core -pkg result -tag thread -tag debug -no-links parser.cmo main.cmo && \
cd _build && \
utop -init "../.ocamlinit"