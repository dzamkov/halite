module UniqueSet = Set.Make (Unique)
module UniqueMap = Map.Make (Unique)

type axiom =
	| Type
	| Kind

let default_var_name i = 
	let ch = Char.chr (Char.code 'a' + i) in (* TODO: Handle overflow *)
	String.make 1 ch

let rec drop n li = if n = 0 then li else match li with
	| _ :: tl -> drop (n - 1) tl
	| [] -> []

let rec nth_opt li n = match li with
	| [] -> None
	| hd :: tl -> if n = 0 then Some hd else nth_opt tl (n - 1)

type var =
	| Int of int (* De Bruijn index *)
	| Ext of Unique.t

type t = {
	reduced_head : head }
and head =
	| Var of var
	| Axiom of axiom
	| App of t * t
	| Abs of t * t
	| Prod of t * t
	| TypeOf of t
	| Cast of t * t

type sub =
	| HasValue of t
	| HasType of t

let map_sub func sub = match sub with
	| HasValue value -> HasValue (func value)
	| HasType typ -> HasType (func typ)

type sub_map = sub UniqueMap.t

type context = {
	int_dim : int;
	int_subs : sub list;
	ext_subs : sub_map }

let empty_context = {
	int_dim = 0;
	int_subs = [];
	ext_subs = UniqueMap.empty }

let rec eq a b = a == b || match (a.reduced_head, b.reduced_head) with
	| (Var a, Var b) -> a = b
	| (Axiom a_ax, Axiom b_ax) -> a_ax = b_ax
	| (App (a_f, a_a), App (b_f, b_a)) -> eq a_f b_f && eq a_a b_a
	| (Abs (a_a, a_b), Abs (b_a, b_b)) -> eq a_a b_a && eq a_b b_b
	| (Prod (_, a_r), Prod (_, b_r)) -> eq a_r b_r
	| (TypeOf a, TypeOf b) -> eq a b
	| (Cast (a_v, a_t), Cast (b_v, b_t)) -> eq a_v b_v && eq a_t b_t
	| _ -> false

let reduced_head expr = expr.reduced_head

let rec _occurs pred offset expr = match reduced_head expr with
	| Var (Int i) -> (i >= offset) && pred (Int (i - offset))
	| Var (Ext _ as v) -> pred v
	| Axiom _ -> false
	| App (f, a) -> _occurs pred offset f || _occurs pred offset a
	| Abs (a, b) -> _occurs pred offset a || _occurs pred (offset + 1) b
	| Prod (_, r) -> _occurs pred offset r
	| TypeOf e -> _occurs pred offset e
	| Cast (v, t) -> _occurs pred offset v || _occurs pred offset t

let occurs pred = _occurs pred 0

let occurs_int i = occurs (fun var -> var = Int i)

let occurs_ext_set set = occurs (fun var -> match var with
	| Int _ -> false
	| Ext e -> UniqueSet.mem e set)

let occurs_ext_map_key map = occurs (fun var -> match var with
	| Int _ -> false
	| Ext e -> UniqueMap.mem e map)

let from_reduced_head head = {
	reduced_head = head }

let rec _shift level amount expr = match reduced_head expr with
	| Var (Int i) when i >= level -> from_reduced_head (Var (Int (i + amount)))
	| Var _ -> expr
	| Axiom _ -> expr
	| App (f, a) ->
		let n_f = _shift level amount f in
		let n_a = _shift level amount a in
		if f == n_f && a == n_a then expr else from_reduced_head (App (n_f, n_a))
	| Abs (a, b) ->
		let n_a = _shift level amount a in
		let n_b = _shift (level + 1) amount b in
		if a == n_a && b == n_b then expr else from_reduced_head (Abs (n_a, n_b))
	| Prod (a, r) ->
		let n_a = _shift level amount a in
		let n_r = _shift level amount r in
		if a == n_a && r == n_r then expr else from_reduced_head (Prod (n_a, n_r))
	| TypeOf e ->
		let n_e = _shift level amount e in
		if e == n_e then expr else from_reduced_head (TypeOf n_e)
	| Cast (v, t) ->
		let n_v = _shift level amount v in
		let n_t = _shift level amount t in
		if v == n_v && t == n_t then expr else from_reduced_head (Cast (n_v, n_t))

let shift level amount expr = if amount = 0 then expr else _shift level amount expr

let rec rebuild context expr = match reduced_head expr with
	| Var (Int i) -> (match nth_opt context.int_subs i with
		| Some (HasValue value) -> shift 0 (i + 1) value
		| _ -> expr)
	| Var (Ext e) ->
		(try match UniqueMap.find e context.ext_subs with
			| HasType _ -> expr
			| HasValue value -> shift 0 context.int_dim value
		with Not_found -> expr)
	| Axiom _ -> expr
	| App (f, a) ->
		let n_f = rebuild context f in
		let n_a = rebuild context a in
		if eq f n_f && eq a n_a then expr else _app context n_f n_a
	| Abs (a, b) ->
		let n_a = rebuild context a in
		let n_context = {
			int_dim = context.int_dim + 1;
			int_subs = HasType a :: context.int_subs;
			ext_subs = context.ext_subs } in
		let n_b = rebuild n_context b in
		if eq a n_a && eq b n_b then expr else _abs n_a n_b
	| Prod (a, r) -> (* TODO: Performance optimization by special case with r as Abs *)
		let n_a = rebuild context a in
		let n_r = rebuild context r in
		if eq a n_a && eq r n_r then expr else _prod n_a n_r
	| TypeOf e ->
		let n_expr = _typeof context e in
		if eq expr n_expr then expr else n_expr
	| Cast (v, t) ->
		let n_v = rebuild context v in
		let n_t = rebuild context t in
		if eq v n_v && eq t n_t then expr else cast n_v n_t

and var i = from_reduced_head (Var i)

and ax ax = from_reduced_head (Axiom ax)

and _app context f a = match reduced_head f with
| Abs (_, f_b) ->
	let n_context = {
		int_dim = context.int_dim + 1;
		int_subs = HasValue a :: context.int_subs;
		ext_subs = context.ext_subs } in
	shift 1 (-1) (rebuild n_context f_b)
| _ -> from_reduced_head (App (f, a))

and _abs arg_typ body = match reduced_head body with
	| App (b_f, b_a) -> (match b_a.reduced_head with
		| Var (Int 0) when not (occurs_int 0 b_f) -> shift 1 (-1) b_f
		| _ -> from_reduced_head (Abs (arg_typ, body)))
	| _ -> from_reduced_head (Abs (arg_typ, body))

and _prod arg_typ res_typ = from_reduced_head (Prod (arg_typ, res_typ))

and _typeof context expr = match reduced_head expr with
	| Var (Int i) -> (match nth_opt context.int_subs i with
		| Some (HasType t) -> shift 0 (i + 1) t
		| Some (HasValue v) ->
			let n_context = {
				int_dim = context.int_dim - (i + 1);
				int_subs = drop (i + 1) context.int_subs;
				ext_subs = context.ext_subs }
			in shift 0 (i + 1) (_typeof n_context v)
		| None -> from_reduced_head (TypeOf expr))
	| Var (Ext e) ->
		(try match UniqueMap.find e context.ext_subs with
			| HasType t -> shift 0 context.int_dim t
			| HasValue v -> shift 0 context.int_dim (_typeof empty_context v)
		with Not_found -> from_reduced_head (TypeOf expr))
	| Axiom Type -> ax Kind
	| App (f, a) -> (match reduced_head (_typeof context f) with
		| Prod (_, r) -> _app context r a
		| _ -> from_reduced_head (TypeOf expr))
	| Abs (a, b) ->
		let n_context = {
			int_dim = context.int_dim + 1;
			int_subs = HasType a :: context.int_subs;
			ext_subs = context.ext_subs }
		in _prod a (_abs a (_typeof n_context b))
	| Prod (a, r) ->
		let a_t = _typeof context a in
		let r_t = _typeof context (_app context r (var (Int 0))) in
		(match (reduced_head a_t, reduced_head r_t) with
			| (Axiom Type, Axiom Type) -> ax Type
			| (Axiom Kind, Axiom Type) -> ax Type
			| (Axiom Type, Axiom Kind) -> ax Kind
			| (Axiom Kind, Axiom Kind) -> ax Kind
			| _ -> from_reduced_head (TypeOf expr))
	| Cast (_, t) -> t
	| _ -> from_reduced_head (TypeOf expr)

and _trivial_typ_func expr offset (u : Unique.t) = from_reduced_head (TypeOf expr)

and cast v t = from_reduced_head (Cast (v, t))

let var_ext u = var (Ext u)

let app = _app empty_context

let subs_map (map : sub_map) = rebuild {
	int_dim = 0;
	int_subs = [];
	ext_subs = map }

let shift_subs_map amount (map : sub_map) expr = rebuild {
	int_dim = amount;
	int_subs = [];
	ext_subs = map } (shift 0 amount expr)

let sub_map_add key (sub : sub) (map : sub_map) : sub_map =
	let s_map = UniqueMap.singleton key sub in
	let n_map = UniqueMap.map (map_sub (subs_map s_map)) map in
	UniqueMap.add key sub n_map

let abs arg_typ func =
	let u = Unique.create () in
	let _body = shift 0 1 (func (var (Ext u))) in
	let body = subs_map (UniqueMap.singleton u (HasValue (var (Int 0)))) _body in
	_abs arg_typ body

let prod arg_typ func = _prod arg_typ (abs arg_typ func)

let const arg_typ value = _abs arg_typ (shift 0 1 value)

let prod_const arg_typ value = _prod arg_typ (const arg_typ value)

let typeof = _typeof empty_context

let rec to_ast var_name level expr = match reduced_head expr with
	| Var (Int i) when i < level -> Grammar.Ident (var_name (level - i - 1))
	| Var (Int i) -> Grammar.Prefix ("$", Grammar.Num (Num.num_of_int i))
	| Var (Ext e) -> Grammar.Prefix ("%", Grammar.Num (Num.num_of_int e))
	| Axiom Type -> Grammar.Ident "type"
	| Axiom Kind -> Grammar.Ident "kind"
	| App (f, a) -> Grammar.App (to_ast var_name level f, to_ast var_name level a)
	| Abs (a, b) ->
		Grammar.Abs (
			Grammar.Typed (Grammar.Ident (var_name level), to_ast var_name level a),
			to_ast var_name (level + 1) b)
	| Prod (a, r) ->
		let a_ast = to_ast var_name level a in
		let n_r = app (shift 0 1 r) (var (Int 0)) in
		Grammar.Infix (
			(if occurs_int 0 n_r
			then Grammar.Collection (Grammar.Set, [
				Grammar.Typed (Grammar.Ident (var_name level), a_ast)])
			else a_ast), [("->", to_ast var_name (level + 1) n_r)])
	| TypeOf e -> Grammar.App (Grammar.Ident "typeof", to_ast var_name level e)
	| Cast (v, t) -> Grammar.Typed (
			to_ast var_name level v,
			to_ast var_name level t)
	
let pp_print formatter expr =
	Grammar.pp_print_expr formatter
		(to_ast default_var_name 0 expr)