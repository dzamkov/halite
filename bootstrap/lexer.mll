{
	open Parser
	open Lexing
}

let ident_start = ['a'-'z' 'A'-'Z' '_']
let ident_main = ident_start | ['0'-'9']
let ident = ident_start ident_main*
let num = (['1'-'9'] ['0'-'9']* | '0') ('.' ['0' - '9']+)?

let op_main = ['!' '$' '%' '&' '*' '+' '-' '/' ':' ';' '<' '=' '>' '?' '@' '^' '|' '~']
let op = op_main op_main*

rule token = parse
	| [' ' '\r' '\t']+ { SPACE }
	| ['\n'] { new_line lexbuf; SPACE }
	| "--" [^ '\n']* { SPACE }
	| eof { EOF }
	| "(" { LROUND }
	| ")" { RROUND }
	| "[" { LSQUARE }
	| "]" { RSQUARE }
	| "{" { LCURLY }
	| "}" { RCURLY }
	| "." { DOT }
	| "," { COMMA }
	| op {
		match lexeme lexbuf with
		| ":" -> COLON
		| "=" -> EQUAL
		| op -> OP op }
	| ident {
		match lexeme lexbuf with
		| "let" -> LET
		| "open" -> OPEN
		| "in" -> IN
		| "begin" -> BEGIN
		| "record" -> RECORD
		| "struct" -> STRUCT
		| "end" -> END
		| "val" -> VAL
		| "include" -> INCLUDE
		| ident -> IDENT ident }
	| num { NUM (Num.num_of_string (lexeme lexbuf)) }
	| _ { raise (Grammar.LexError (lexeme_start_p lexbuf)) }