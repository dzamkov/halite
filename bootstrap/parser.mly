%token LROUND RROUND
%token LSQUARE RSQUARE
%token LCURLY RCURLY
%token DOT COMMA COLON EQUAL
%token LET OPEN IN
%token BEGIN RECORD STRUCT END
%token VAL INCLUDE
%token <string> OP
%token <string> IDENT
%token <Num.num> NUM
%token SPACE EOF
%start main
%type <Grammar.Inst.expr> main
%{ open Grammar.Inst %}
%%

main:
	| space? expr_and(space?) EOF { $2 }
	| error { raise (Grammar.ParseError $startpos) }
space:
	| SPACE+ { () }
	
ident:
	| IDENT { sym $1 $startpos $endpos }
op:
	| OP { sym $1 $startpos $endpos }

%inline atom_expr:
	| ident { ident $1 $startpos $endpos }
	| atom_n_ident_expr { $1 }
atom_n_ident_expr:
	| NUM { num $1 $startpos $endpos }
	| LROUND separated_list(COMMA, space? expr_and(space?) { $2 }) RROUND { match $2 with
		| [expr] -> expr
		| li -> collection Model.Tuple li $startpos $endpos }
	| LSQUARE separated_list(COMMA, space? expr_and(space?) { $2 }) RSQUARE { collection Model.List $2 $startpos $endpos }
	| LCURLY separated_list(COMMA, space? expr_and(space?) { $2 }) RCURLY { collection Model.Set $2 $startpos $endpos }
sub_expr:
	| atom_expr list(DOT atom_expr { ($2, $endpos) }) {
		List.fold_left
			(fun accum (i, e) -> sub accum true i $startpos e)
			$1 $2 }
tight_expr:
	| sub_expr { $1 }
	| op sub_expr { prefix $1 $2 $startpos $endpos }
	| sub_expr op { postfix $1 $2 $startpos $endpos }
	| sub_expr nonempty_list(OP sub_expr { (sym $1 $startpos($1) $endpos($1), $2) }) { infix $1 $2 $startpos $endpos }
app_expr:
	| tight_expr { $1 }
	| app_expr space tight_expr { app $1 $3 $startpos $endpos }
%inline word_expr:
	| app_expr { $1 }
	| word_n_app_expr { $1 }
infix_expr_list_and(X):
	| word_expr X { ($1, []) }
	| word_expr space OP space infix_expr_list_and(X) { let (h, t) = $5 in ($1, ((sym $3 $startpos($3) $endpos($3), h) :: t)) }
infix_expr_and(X):
	| infix_expr_list_and(X) { let (h, t) = $1 in match t with
		| [] -> h
		| t -> infix h t $startpos $endpos }
scope_dir_and_space:
	| LET space pattern_and_space EQUAL space long_expr_and(space) { Model.Let ($3, $6) }
	| OPEN space long_expr_and(space) { Model.Open $3 }
long_expr_and(X):
	| infix_expr_and(X) { $1 }
	| infix_expr_and(space) COLON space long_expr_and(X) { typed $1 $4 $startpos $endpos }
	| atom_expr DOT space long_expr_and(X) { abs $1 $4 $startpos $endpos }
	| scope_dir_and_space IN space long_expr_and(X) { scope_in $1 $4 $startpos $endpos }
expr_and(X):
	| long_expr_and(X) { $1 }
word_n_app_expr:
	| BEGIN space expr_and(space) END { $3 }
	(* | RECORD block END { record $2 $startpos $endpos }
	| STRUCT block END { struc $2 $startpos $endpos } *)

pattern_infix_part:
	| atom_expr space op space atom_expr { Model.PInfix ($1, $3, $5) }
%inline pattern_tight_part:
	| ident { Model.PIdent $1 }
	| op atom_expr { Model.PPrefix ($1, $2) }
	| atom_expr op { Model.PPostfix ($1, $2) }
pattern_app_part_and_space:
	| pattern_tight_part space { $1 }
	| pattern_app_part_and_space atom_expr space { Model.PApp ($1, $2) }
pattern_part_and_space:
	| pattern_infix_part space { $1 }
	| pattern_app_part_and_space { $1 }
pattern_and_space:
	| pattern_part_and_space { pattern $1 None $startpos($1) $endpos($1) }
	| pattern_part_and_space COLON space infix_expr_and(space) { pattern $1 (Some $4) $startpos($1) $endpos($1) }

(*
block:
	| space list(dir_and_space) { $2 }
dir_and_space:
	| INCLUDE space expr_and(space) { Include $3 }
	| VAL space pattern_and_space { Val ($3, None, None) }
	| VAL space pattern_and_space COLON space long_expr_and(space) { Val ($3, Some $6, None) }
	| VAL space pattern_and_space EQUAL space long_expr_and(space) { Val ($3, None, Some $6) }
	| VAL space pattern_and_space COLON space infix_expr_and(space) EQUAL space long_expr_and(space) { Val ($3, Some $6, Some $9) }
	| scope_dir_and_space { ScopeDir $1 }
	*)