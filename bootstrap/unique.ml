type t = int

let _next : t ref = ref 0

let create () =
    let res = !_next in
    _next := res + 1; res

let compare : t -> t -> int = compare