open Grammar

let parse_lexbuf lexbuf =
  Parser.main Lexer.token lexbuf

let parse_channel channel =
  parse_lexbuf (Lexing.from_channel channel)

let parse_string str =
  parse_lexbuf (Lexing.from_string str)

let parse_file name =
  let channel = open_in name in
  let result = parse_channel channel in
  close_in channel; result