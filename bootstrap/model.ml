module UniqueSet = Set.Make (Unique)
module UniqueMap = Map.Make (Unique)

type collection_type =
	| Tuple
	| List
	| Set

type ('expr, 'pattern) _scope_directive =
	| Let of 'pattern * 'expr
	| Open of 'expr

type ('sym, 'expr, 'pattern, 'block) _expr =
	| Ident of 'sym
	| Num of Num.num
	| Sub of 'expr * bool * 'expr
	| App of 'expr * 'expr
	| Abs of 'expr * 'expr
	| Typed of 'expr * 'expr
	| Prefix of 'sym * 'expr
	| Postfix of 'expr * 'sym
	| Infix of 'expr * ('sym * 'expr) list
	| In of ('expr, 'pattern) _scope_directive * 'expr
	| Collection of collection_type * 'expr list
	| Record of 'block
	| Struct of 'block

type ('expr, 'pattern) _directive =
	| Include of 'expr
	| ScopeDir of ('expr, 'pattern) _scope_directive
	| Val of 'pattern * 'expr option

type ('sym, 'expr) _pattern =
	| PIdent of 'sym
	| PPrefix of 'sym * 'expr
	| PPostfix of 'expr * 'sym
	| PInfix of 'expr * 'sym * 'expr 
	| PApp of ('sym, 'expr) _pattern * 'expr

type ('sym, 'term) _term =
	| Var of int
	| MetaVar of Unique.t
	| TApp of 'term * 'term
	| TAbs of 'sym * 'term * 'term
	| TProd of 'sym * 'term * 'term
	| TypeOf of 'term
	| Cast of 'term * 'term

type axiom =
	| Type
	| Kind

(* Syntactic types where context is not available *)
type free_sym = string
type free_expr = FreeE of (free_sym, free_expr, free_pattern, free_block) _expr
and free_pattern = free_expr * free_expr option
and free_block = (free_expr, free_pattern) _directive list

(* Semantic types where context is not available *)
type free_term = FreeT of (unit, free_term) _term

(* Syntactic types where context is available *)
type inst_sym = {
	sym : string;
	sym_start_pos : Lexing.position;
	sym_end_pos : Lexing.position;
	mutable sym_def : inst_sym option }
and inst_expr = {
	expr : (inst_sym, inst_expr, inst_pattern, inst_block) _expr;
	expr_start_pos : Lexing.position;
	expr_end_pos : Lexing.position;
	mutable expr_value : inst_term }
and inst_pattern = {
	pattern : (inst_sym, inst_expr) _pattern;
	pattern_typ : inst_expr option;
	pattern_start_pos : Lexing.position;
	pattern_end_pos : Lexing.position }
and inst_block = inst_directive list
and inst_directive = {
	directive : (inst_expr, inst_pattern) _directive;
	directive_start_pos : Lexing.position }

(* Semantic types where context is available *)
and inst_term = {
	reduced : (inst_sym option, inst_term) _term;
	source : inst_expr option }