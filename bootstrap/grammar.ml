open Result
open Model

exception LexError of Lexing.position
exception ParseError of Lexing.position

type syntax_level =
	| LAtom
	| LSub
	| LTight
	| LApp
	| LInfix
	| LLong

module Free = struct
	type sym = free_sym
	type expr = free_expr
	type pattern = free_pattern
	type block = free_block

	let ident (sym : sym) : expr = FreeE (Ident sym)

	let num num : expr = FreeE (Num num)

	let sub (container : expr) exp (sub : expr) : expr = FreeE (Sub (container, exp, sub))

	let app (f : expr) (arg : expr) : expr = FreeE (App (f, arg))

	let abs (arg : expr) (body : expr) : expr = FreeE (Abs (arg, body))

	let typed (v : expr) (t : expr) : expr = FreeE (Typed (v, t))

	let prefix (op : sym) (arg : expr) : expr = FreeE (Prefix (op, arg))

	let postfix (arg : expr) (op : sym) : expr = FreeE (Postfix (arg, op))

	let infix (head : expr) (rem : (sym * expr) list) : expr = FreeE (Infix (head, rem))

	let scope_in dir (body : expr) : expr = FreeE (In (dir, body))

	let collection typ (items : expr list) : expr = FreeE (Collection (typ, items))
	
	let record (block : block) : expr = FreeE (Record block)

	let struc (block : block) : expr = FreeE (Struct block)


	let indent = 2

	let pp_with_level target current formatter inner =
		if compare target current > 0 then
			(Format.pp_open_box formatter indent;
			Format.pp_print_string formatter "(";
			inner formatter;
			Format.pp_print_string formatter ")";
			Format.pp_close_box formatter ())
		else inner formatter

	let rec pp_print_expr_level level formatter (FreeE expr : expr) = match expr with
		| Ident ident -> Format.pp_print_string formatter ident
		| Num value -> Format.pp_print_string formatter (Num.string_of_num value)
		| Sub (container, exp, sub) -> pp_with_level LSub level formatter (fun formatter ->
			pp_print_expr_level LSub formatter container;
			Format.pp_print_cut formatter ();
			Format.pp_print_string formatter ".";
			if exp then pp_print_expr_level LAtom formatter sub else
				(Format.pp_open_box formatter indent;
				Format.pp_print_string formatter "(";
				pp_print_expr_level LLong formatter sub;
				Format.pp_print_string formatter ")";
				Format.pp_close_box formatter ()))
		| App (f, arg) -> pp_with_level LApp level formatter (fun formatter ->
			pp_print_expr_level LApp formatter f;
			Format.pp_print_space formatter ();
			pp_print_expr_level LTight formatter arg)
		| Abs (arg, body) -> pp_with_level LLong level formatter (fun formatter ->
			pp_print_expr_level LAtom formatter arg;
			Format.pp_print_string formatter ".";
			Format.pp_print_space formatter ();
			pp_print_expr_level LLong formatter body)
		| Typed (expr, typ) -> pp_with_level LLong level formatter (fun formatter ->
			pp_print_expr_level LInfix formatter expr;
			Format.pp_print_string formatter " :";
			Format.pp_print_space formatter ();
			pp_print_expr_level LLong formatter typ)
		| Prefix (op, inner) -> pp_with_level LTight level formatter (fun formatter ->
			Format.pp_print_string formatter op;
			pp_print_expr_level LSub formatter inner)
		| Postfix (inner, op) -> pp_with_level LTight level formatter (fun formatter ->
			pp_print_expr_level LSub formatter inner;
			Format.pp_print_string formatter op)
		| Infix (head, parts) -> pp_with_level LInfix level formatter (fun formatter ->
			pp_print_expr_level LApp formatter head;
			List.iter (fun (op, oper) ->
				Format.pp_print_string formatter " ";
				Format.pp_print_string formatter op;
				Format.pp_print_space formatter ();
				pp_print_expr_level LApp formatter oper) parts)
		| In (dir, expr) -> pp_with_level LLong level formatter (fun formatter ->
			pp_print_scope_directive formatter dir;
			Format.pp_print_space formatter ();
			Format.pp_print_string formatter "in ";
			pp_print_expr_level LLong formatter expr)
		| Collection (typ, items) ->
			let (s, e) = (match typ with
				| Tuple -> ("(", ")")
				| List -> ("[", "]")
				| Set -> ("{", "}")) in
			Format.pp_open_box formatter indent;
			Format.pp_print_string formatter s;
			(match items with
				| h :: t ->
					pp_print_expr_level LLong formatter h;
					List.iter (fun item ->
						Format.pp_print_string formatter ",";
						Format.pp_print_space formatter ();
						pp_print_expr_level LLong formatter item) t
				| [] -> ());
			Format.pp_print_string formatter e;
			Format.pp_close_box formatter ()
		| Record block ->
			Format.pp_open_hvbox formatter 0;
			Format.pp_print_string formatter "record";
			Format.pp_open_vbox formatter (indent - String.length "record");
			pp_print_block formatter block;
			Format.pp_close_box formatter ();
			Format.pp_print_space formatter ();
			Format.pp_print_string formatter "end";
			Format.pp_close_box formatter ()
		| Struct block ->
			Format.pp_open_hvbox formatter 0;
			Format.pp_print_string formatter "struct";
			Format.pp_open_vbox formatter (indent - String.length "struct");
			pp_print_block formatter block;
			Format.pp_close_box formatter ();
			Format.pp_print_space formatter ();
			Format.pp_print_string formatter "end";
			Format.pp_close_box formatter ()

	and pp_print_pattern formatter ((def, typ) : pattern) =
		pp_print_expr_level LInfix formatter def;
		(match typ with
			| Some typ ->
				Format.pp_print_string formatter " :";
				Format.pp_print_space formatter ();
				pp_print_expr_level LInfix formatter typ;
			| None -> ());

	and pp_print_scope_directive formatter dir = match dir with
		| Let (pattern, value) ->
			Format.pp_open_box formatter indent;
			Format.pp_print_string formatter "let ";
			pp_print_pattern formatter pattern;
			Format.pp_print_string formatter " =";
			Format.pp_print_space formatter ();
			pp_print_expr_level LLong formatter value;
			Format.pp_close_box formatter ();
		| Open expr ->
			Format.pp_open_box formatter indent;
			Format.pp_print_string formatter "open ";
			pp_print_expr_level LLong formatter expr;
			Format.pp_close_box formatter ()

	and pp_print_directive formatter dir = match dir with
		| Include expr ->
			Format.pp_open_box formatter indent;
			Format.pp_print_string formatter "include ";
			pp_print_expr_level LLong formatter expr;
			Format.pp_close_box formatter ()
		| ScopeDir dir -> pp_print_scope_directive formatter dir
		| Val (pattern, value) ->
			Format.pp_open_box formatter indent;
			Format.pp_print_string formatter "let ";
			pp_print_pattern formatter pattern;
			(match value with
				| Some value ->
					Format.pp_print_string formatter " =";
					Format.pp_print_space formatter ();
					pp_print_expr_level LLong formatter value
				| None -> ());
			Format.pp_close_box formatter ()

	and pp_print_block formatter (block : block) =
			List.iter (fun dir ->
				Format.pp_print_space formatter ();
				pp_print_directive formatter dir) block

	let pp_print_expr = pp_print_expr_level LLong

	let pp_print_expr_value formatter expr =
		Format.pp_print_string formatter "parse_string \"";
		pp_print_expr formatter expr;
		Format.pp_print_string formatter "\""
end

module Inst = struct
	type sym = inst_sym
	type expr = inst_expr
	type pattern = inst_pattern
	type block = inst_block
	type directive = inst_directive

	let sym str s e : sym = {
		sym = str;
		sym_start_pos = s;
		sym_end_pos = e;
		sym_def = None }

	let expr value s e : expr =
		let rec expr = {
			expr = value;
			expr_start_pos = s;
			expr_end_pos = e;
			expr_value = term }
		and term = {
			reduced = MetaVar (Unique.create ());
			source = Some expr }
		in expr

	let pattern value typ s e : pattern = {
		pattern = value;
		pattern_typ = typ;
		pattern_start_pos = s;
		pattern_end_pos = e }

	let ident (sym : sym) = expr (Ident sym)

	let num num = expr (Num num)

	let sub (container : expr) exp (sub : expr) = expr (Sub (container, exp, sub))

	let app (f : expr) (arg : expr) = expr (App (f, arg))

	let abs (arg : expr) (body : expr) = expr (Abs (arg, body))

	let typed (v : expr) (t : expr) = expr (Typed (v, t))

	let prefix (op : sym) (arg : expr) = expr (Prefix (op, arg))

	let postfix (arg : expr) (op : sym) = expr (Postfix (arg, op))

	let infix (head : expr) (rem : (sym * expr) list) = expr (Infix (head, rem))

	let scope_in dir (body : expr) = expr (In (dir, body))

	let collection typ (items : expr list) = expr (Collection (typ, items))
	
	let record (block : block) = expr (Record block)

	let struc (block : block) = expr (Struct block)


	let sym_to_free (sym : sym) : Free.sym = sym.sym

	let rec expr_to_free (expr : expr) : Free.expr = match expr.expr with
		| Ident sym -> Free.ident (sym_to_free sym)
		| Num num -> Free.num num
		| Sub (container, exp, sub) ->
			Free.sub (expr_to_free container) exp (expr_to_free sub)
		| App (f, arg) -> Free.app (expr_to_free f) (expr_to_free arg)
		| Abs (arg, body) -> Free.abs (expr_to_free arg) (expr_to_free body)
		| Typed (v, t) -> Free.typed (expr_to_free v) (expr_to_free t)
		| Prefix (op, arg) -> Free.prefix (sym_to_free op) (expr_to_free arg)
		| Postfix (arg, op) -> Free.postfix (expr_to_free arg) (sym_to_free op)
		| Infix (head, rem) ->
			Free.infix
				(expr_to_free head)
				(List.map (fun (op, arg) -> (sym_to_free op, expr_to_free arg)) rem)
		| In (scope_dir, expr) ->
			Free.scope_in (_scope_directive_to_free scope_dir) (expr_to_free expr)
		| Collection (t, exprs) -> Free.collection t (List.map expr_to_free exprs)
	
	and pattern_to_free (pattern : pattern) : Free.pattern =
		(_pattern_to_free pattern.pattern, match pattern.pattern_typ with
			| Some typ -> Some (expr_to_free typ)
			| None -> None)

	and _pattern_to_free pattern = match pattern with
		| PIdent sym -> Free.ident (sym_to_free sym)
		| PPrefix (op, arg) -> Free.prefix (sym_to_free op) (expr_to_free arg)
		| PPostfix (arg, op) -> Free.postfix (expr_to_free arg) (sym_to_free op)
		| PInfix (l_arg, op, r_arg) ->
			Free.infix
				(expr_to_free l_arg)
				[(sym_to_free op, expr_to_free r_arg)]
		| PApp (f, arg) -> Free.app (_pattern_to_free f) (expr_to_free arg)

	and _scope_directive_to_free dir = match dir with
		| Let (pattern, value) -> Let (pattern_to_free pattern, expr_to_free value)
		| Open expr -> Open (expr_to_free expr)

end