#!/bin/sh

set -e
cd /vagrant/

# Install OCaml
add-apt-repository ppa:avsm/ppa
apt-get -y update
apt-get -y install ocaml ocaml-native-compilers camlp4-extra opam m4

# Initialize environment
su vagrant <<EOF
	opam init -y
	opam install -y ocamlfind utop core result menhir
	echo "eval \`opam config env\`" >> ~/.bashrc
EOF